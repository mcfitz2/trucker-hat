SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

cd $SCRIPT_DIR/../
flask --app trucker_hat/backend/app.py spec > $SCRIPT_DIR/openapi.yaml
cd $SCRIPT_DIR
rm -rf $SCRIPT_DIR/../trucker_hat/client
rm -rf api-flask-client
openapi-python-client generate --path openapi.yaml
mv api-flask-client/api_flask_client $SCRIPT_DIR/../trucker_hat/client
rm -rf api-flask-client
rm openapi.yaml
