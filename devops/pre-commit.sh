SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

cd $SCRIPT_DIR/../ || exit
isort trucker_hat
isort main.py

black trucker_hat
black main.py