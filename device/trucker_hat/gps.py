import pprint
from threading import Thread

import pynmea2
import serial


class GPSMonitor:
    def __init__(self, serial_port, baud_rate=9600):
        self.serial_port = serial_port
        self.baud_rate = baud_rate

    def start(self):
        serial_thread = Thread(target=self._run)
        serial_thread.start()

    def _run(self):
        with serial.Serial(self.serial_port,
                           baudrate=self.baud_rate,
                           timeout=1) as ser:
            while True:
                line = ser.readline().decode("ascii", errors="replace")
                nmea = line.strip()
                nmeaobj = pynmea2.parse(nmea)
                pprint.pprint(
                    [
                        "%s: %s" % (nmeaobj.fields[i][0], nmeaobj.data[i])
                        for i in range(len(nmeaobj.fields))
                    ]
                )
