import sys
import time
from threading import Thread

import can

PID_REQUEST = 0x7DF
PID_REPLY = 0x7E8


class CanMonitor:
    def __init__(self, **kwargs):
        self.callbacks = []
        self.bus = None
        self.request_interval = 10
        try:
            self.bus = can.Bus(**kwargs)
        except OSError:
            print("Failed to create bus")
            sys.exit(1)

    def watch(self, pid, request=True):
        def decorate(f):
            t = (pid, request, f)
            self.callbacks.append(t)
            return f

        return decorate

    def tx_task(self):
        while True:
            for pid, request, _ in self.callbacks:
                if request:
                    msg = can.Message(
                        arbitration_id=PID_REQUEST,
                        data=[0x02, 0x01, pid, 0x00, 0x00, 0x00, 0x00, 0x00],
                    )
                    self.bus.send(msg)
                    time.sleep(self.request_interval)

    def rx_task(self):
        while True:
            if self.bus:
                message = self.bus.recv()
                if message.arbitration_id == PID_REPLY:
                    for pid, _, callback in self.callbacks:
                        if pid == message.data[2]:
                            callback(message)

    def start(self):
        rx_task_thread = Thread(target=self.rx_task)
        rx_task_thread.start()
        self.tx_task()
