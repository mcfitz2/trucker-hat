import paho.mqtt.client as paho


class MqttClient(paho.Client):
    def __init__(self, host, port, username=None, password=None):
        paho.Client.__init__(self, paho.CallbackAPIVersion.VERSION2)
        self.host = host
        self.port = port
        self.username = username
        self.password = password
        self.connect_callbacks = []
        self.disconnect_callbacks = []
        self.topic_callbacks = []
        self.connected = False
        self.on_connect = self.connect_callback
        self.on_disconnect = self.disconnect_callback
        if self.username and self.password:
            self.username_pw_set(username=self.username,
                                 password=self.password)

        self.connect(self.host, self.port, 60)

    def is_connected(self):
        return self.connected

    def connect_callback(self, client, userdata, flags, reason_code, props):
        print(f"Connected with result code {reason_code}")
        self.connected = True
        for topic, _ in self.topic_callbacks:
            self.subscribe(topic)
        for cb in self.connect_callbacks:
            cb()

    def disconnect_callback(self, client, userdata, flags, reason_code):
        print(f"Disconnected with result code {reason_code}")
        self.connected = False
        for cb in self.disconnect_callbacks:
            cb()

    def add_connection_callbacks(self, c_cb, d_cb):
        self.connect_callbacks.append(c_cb)
        self.disconnect_callbacks.append(d_cb)

    def add_callback(self, topic, t_cb):
        t = (topic, t_cb)
        self.topic_callbacks.append(t)
        self.subscribe(topic)

    def on_message(self, client, userdata, msg):
        for topic, cb in self.topic_callbacks:
            if topic == msg.topic:
                cb(topic, msg.payload.decode("utf-8"))
