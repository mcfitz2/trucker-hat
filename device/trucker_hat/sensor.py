import os
import pickle
import time
from threading import Thread

import paho.mqtt.client as paho


def miles_to_km(value):
    return round(float(value) * 1.60934, 0)


def km_to_miles(value):
    return round(float(value) / 1.60934, 0)


class Sensor:
    def __init__(
        self,
        client: paho.Client,
        unique_id,
        name,
        icon=None,
        initial_state=None,
        convert_from_mqtt=float,
        convert_to_mqtt=str,
        enable_discovery=False,
        periodic_send=30,
        sensor_type="sensor",
        discovery_prefix="homeassistant",
        data_dir=".",
    ):
        self.client = client
        self.sensor_type = sensor_type
        self.convert_from_mqtt = convert_from_mqtt
        self.convert_to_mqtt = convert_to_mqtt
        self.name = name
        self.unique_id = unique_id
        self.topic = f"{discovery_prefix}/{sensor_type}/{self.unique_id}"
        self.discovery_prefix = discovery_prefix
        self.icon = icon
        self.enable_discovery = enable_discovery
        self.state = initial_state
        self.data_file = os.path.join(data_dir, f"{self.unique_id}.pickle")
        self.previous_values = []
        self.load_from_disk()
        self.periodic_send = periodic_send
        self.periodic_send_thread = None
        self.heartbeat_thread = None
        self.client.add_connection_callbacks(self.on_connect,
                                             self.on_disconnect)
        self.client.add_callback(f"{self.topic}/state/set",
                                 self._update_from_mqtt)
        self.client.add_callback(f"{discovery_prefix}/status", self._birth)
        self.client.will_set(f"{self.topic}/status", "offline",
                             qos=1,
                             retain=True)

    def save_to_disk(self):
        payload = {
            "state": self.state,
            "previous_values": self.previous_values
        }
        with open(self.data_file, "wb") as f:
            pickle.dump(payload, f)

    def load_from_disk(self):
        if os.path.exists(self.data_file):
            with open(self.data_file, "rb") as f:
                payload = pickle.load(f)
                self.state = payload["state"]
                self.previous_values = payload["previous_values"]

    def _birth(self, topic, payload):
        self._heartbeat()

    def _heartbeat(self):
        while True:
            if self.client.is_connected():
                try:
                    self.send_available()
                except Exception as e:
                    print("Failed to send state", e)
            else:
                print("client is disconnected, not sending config")
            time.sleep(30)

    def _periodic_send(self):
        print(f"Sending value for {self.name} every {self.periodic_send}s")
        while True:
            if self.client.is_connected():
                try:
                    self.send()
                except Exception as e:
                    print("Failed to send state", e)
            else:
                print("client is disconnected, not sending state")
            time.sleep(self.periodic_send)

    def on_connect(self):
        if not self.periodic_send_thread:
            if self.periodic_send and self.periodic_send > 0:
                self.periodic_send_thread = Thread(target=self._periodic_send)
                self.periodic_send_thread.start()
        if not self.heartbeat_thread:
            if self.heartbeat_thread:
                self.heartbeat_thread = Thread(target=self._heartbeat)
                self.heartbeat_thread.start()
        self.send_available()
        self.send()

    def on_disconnect(self):
        self.send_not_available()

    def _update_from_mqtt(self, topic, payload):
        self.state = self.convert_from_mqtt(payload)
        print(f"Got set request: {self.state}")
        self.send()

    def update_state(self, state):
        self.previous_values.append(state)
        self.state = state
        self.send()
        while len(self.previous_values) > 10:
            self.previous_values.pop(0)
        self.save_to_disk()

    def get_previous_values(self):
        return self.previous_values

    def get_state(self):
        return self.state

    def send(self):
        self.client.publish(
            f"{self.topic}/state", str(self.convert_to_mqtt(self.state))
        )
        print(f"{self.topic}/state", str(self.convert_to_mqtt(self.state)))

    def send_available(self):
        self.client.publish(f"{self.topic}/status", "online", retain=False)

    def send_not_available(self):
        self.client.publish(f"{self.topic}/status", "offline", retain=False)
