import os

import yaml

from device.trucker_hat import MqttClient
from device.trucker_hat.can_monitor import CanMonitor
from device.trucker_hat.sensor import Sensor, km_to_miles, miles_to_km

script_dir = os.path.dirname(os.path.realpath(__file__))
config_file = os.path.join(script_dir, "config.yml")
config = {}
if os.path.exists(config_file):
    with open(config_file, "r") as file:
        config = yaml.safe_load(file)


can_monitor = CanMonitor(**config["can"])
client = MqttClient(**config["mqtt"])

odometer_sensor = Sensor(
    client,
    f"{config['sensor']['prefix']}_odometer",
    "Odometer",
    initial_state=0,
    sensor_type="sensor",
    convert_to_mqtt=km_to_miles,
    convert_from_mqtt=miles_to_km,
)
distance_since_sensor = Sensor(
    client,
    f"{config['sensor']['prefix']}_dst_since_clear",
    "Distance Since Codes Cleared",
    initial_state=0,
    convert_from_mqtt=float,
)
fuel_level_sensor = Sensor(
    client,
    f"{config['sensor']['prefix']}_fuel_level",
    "Fuel Level",
    initial_state=0,
    convert_from_mqtt=float,
)


@can_monitor.watch(0x31)
def distance_since_codes_cleared(message):
    A = message.data[3]
    B = message.data[4]
    new_distance = (256 * A) + B
    if len(distance_since_sensor.get_previous_values()) > 0:
        previous_distance = distance_since_sensor.get_previous_values()[-1]
        current_odom = odometer_sensor.get_state()
        diff = new_distance - previous_distance
        if current_odom:
            if diff >= 0:
                odometer_sensor.update_state(current_odom + diff)
            else:
                odometer_sensor.update_state(current_odom + new_distance)

        else:
            print(f"Odometer value is none, updating to {new_distance}")
            odometer_sensor.update_state(new_distance)
    else:
        print("No previous value stored, not updating odometer")
    distance_since_sensor.update_state(new_distance)


@can_monitor.watch(0x2F)
def fuel_level(message):
    A = message.data[3]
    fuel_level_sensor.update_state((100 / 255) * A)


if __name__ == "__main__":
    client.loop_start()
    can_monitor.start()
