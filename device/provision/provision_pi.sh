PI_HOST=$1
PI_USER=$2
PI_PASSWORD=$3
docker build . --build-arg USER=$PI_USER --build-arg HOST=$PI_HOST --build-arg PASSWORD=$PI_PASSWORD -t trucker_hat_ansible
docker run -it -e HOST=$PI_HOST -e USER=$PI_USER trucker_hat_ansible