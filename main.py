import sys

from trucker_hat.backend import run as run_backend
from trucker_hat.device import run as run_device

if __name__ == "__main__":
    command = sys.argv[1]
    if command == "backend":
        run_backend()
    elif command == "device":
        run_device()
