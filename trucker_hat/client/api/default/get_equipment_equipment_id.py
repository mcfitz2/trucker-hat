from http import HTTPStatus
from typing import Any, Dict, Optional, Union

import httpx

from ... import errors
from ...client import AuthenticatedClient, Client
from ...models.equipment import Equipment
from ...models.http_error import HTTPError
from ...types import Response


def _get_kwargs(
    equipment_id: str,
) -> Dict[str, Any]:
    _kwargs: Dict[str, Any] = {
        "method": "get",
        "url": f"/equipment/{equipment_id}",
    }

    return _kwargs


def _parse_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Optional[Union[Equipment, HTTPError]]:
    if response.status_code == HTTPStatus.OK:
        response_200 = Equipment.from_dict(response.json())

        return response_200
    if response.status_code == HTTPStatus.UNAUTHORIZED:
        response_401 = HTTPError.from_dict(response.json())

        return response_401
    if response.status_code == HTTPStatus.NOT_FOUND:
        response_404 = HTTPError.from_dict(response.json())

        return response_404
    if client.raise_on_unexpected_status:
        raise errors.UnexpectedStatus(response.status_code, response.content)
    else:
        return None


def _build_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Response[Union[Equipment, HTTPError]]:
    return Response(
        status_code=HTTPStatus(response.status_code),
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(client=client, response=response),
    )


def sync_detailed(
    equipment_id: str,
    *,
    client: AuthenticatedClient,
) -> Response[Union[Equipment, HTTPError]]:
    """Get Equipment

    Args:
        equipment_id (str):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[Union[Equipment, HTTPError]]
    """

    kwargs = _get_kwargs(
        equipment_id=equipment_id,
    )

    response = client.get_httpx_client().request(
        **kwargs,
    )

    return _build_response(client=client, response=response)


def sync(
    equipment_id: str,
    *,
    client: AuthenticatedClient,
) -> Optional[Union[Equipment, HTTPError]]:
    """Get Equipment

    Args:
        equipment_id (str):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Union[Equipment, HTTPError]
    """

    return sync_detailed(
        equipment_id=equipment_id,
        client=client,
    ).parsed


async def asyncio_detailed(
    equipment_id: str,
    *,
    client: AuthenticatedClient,
) -> Response[Union[Equipment, HTTPError]]:
    """Get Equipment

    Args:
        equipment_id (str):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[Union[Equipment, HTTPError]]
    """

    kwargs = _get_kwargs(
        equipment_id=equipment_id,
    )

    response = await client.get_async_httpx_client().request(**kwargs)

    return _build_response(client=client, response=response)


async def asyncio(
    equipment_id: str,
    *,
    client: AuthenticatedClient,
) -> Optional[Union[Equipment, HTTPError]]:
    """Get Equipment

    Args:
        equipment_id (str):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Union[Equipment, HTTPError]
    """

    return (
        await asyncio_detailed(
            equipment_id=equipment_id,
            client=client,
        )
    ).parsed
