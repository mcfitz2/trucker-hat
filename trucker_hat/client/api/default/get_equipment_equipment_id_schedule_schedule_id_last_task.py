from http import HTTPStatus
from typing import Any, Dict, Optional, Union

import httpx

from ... import errors
from ...client import AuthenticatedClient, Client
from ...models.http_error import HTTPError
from ...models.task import Task
from ...types import Response


def _get_kwargs(
    equipment_id: str,
    schedule_id: str,
) -> Dict[str, Any]:
    _kwargs: Dict[str, Any] = {
        "method": "get",
        "url": f"/equipment/{equipment_id}/schedule/{schedule_id}/last_task",
    }

    return _kwargs


def _parse_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Optional[Union[HTTPError, Task]]:
    if response.status_code == HTTPStatus.OK:
        response_200 = Task.from_dict(response.json())

        return response_200
    if response.status_code == HTTPStatus.UNAUTHORIZED:
        response_401 = HTTPError.from_dict(response.json())

        return response_401
    if response.status_code == HTTPStatus.NOT_FOUND:
        response_404 = HTTPError.from_dict(response.json())

        return response_404
    if client.raise_on_unexpected_status:
        raise errors.UnexpectedStatus(response.status_code, response.content)
    else:
        return None


def _build_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Response[Union[HTTPError, Task]]:
    return Response(
        status_code=HTTPStatus(response.status_code),
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(client=client, response=response),
    )


def sync_detailed(
    equipment_id: str,
    schedule_id: str,
    *,
    client: AuthenticatedClient,
) -> Response[Union[HTTPError, Task]]:
    """Get Last Task

    Args:
        equipment_id (str):
        schedule_id (str):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[Union[HTTPError, Task]]
    """

    kwargs = _get_kwargs(
        equipment_id=equipment_id,
        schedule_id=schedule_id,
    )

    response = client.get_httpx_client().request(
        **kwargs,
    )

    return _build_response(client=client, response=response)


def sync(
    equipment_id: str,
    schedule_id: str,
    *,
    client: AuthenticatedClient,
) -> Optional[Union[HTTPError, Task]]:
    """Get Last Task

    Args:
        equipment_id (str):
        schedule_id (str):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Union[HTTPError, Task]
    """

    return sync_detailed(
        equipment_id=equipment_id,
        schedule_id=schedule_id,
        client=client,
    ).parsed


async def asyncio_detailed(
    equipment_id: str,
    schedule_id: str,
    *,
    client: AuthenticatedClient,
) -> Response[Union[HTTPError, Task]]:
    """Get Last Task

    Args:
        equipment_id (str):
        schedule_id (str):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[Union[HTTPError, Task]]
    """

    kwargs = _get_kwargs(
        equipment_id=equipment_id,
        schedule_id=schedule_id,
    )

    response = await client.get_async_httpx_client().request(**kwargs)

    return _build_response(client=client, response=response)


async def asyncio(
    equipment_id: str,
    schedule_id: str,
    *,
    client: AuthenticatedClient,
) -> Optional[Union[HTTPError, Task]]:
    """Get Last Task

    Args:
        equipment_id (str):
        schedule_id (str):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Union[HTTPError, Task]
    """

    return (
        await asyncio_detailed(
            equipment_id=equipment_id,
            schedule_id=schedule_id,
            client=client,
        )
    ).parsed
