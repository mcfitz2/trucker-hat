from http import HTTPStatus
from typing import Any, Dict, Optional, Union

import httpx

from ... import errors
from ...client import AuthenticatedClient, Client
from ...models.http_error import HTTPError
from ...models.schedule import Schedule
from ...models.validation_error import ValidationError
from ...types import Response


def _get_kwargs(
    equipment_id: str,
    *,
    body: Schedule,
) -> Dict[str, Any]:
    headers: Dict[str, Any] = {}

    _kwargs: Dict[str, Any] = {
        "method": "post",
        "url": f"/equipment/{equipment_id}/schedule",
    }

    _body = body.to_dict()

    _kwargs["json"] = _body
    headers["Content-Type"] = "application/json"

    _kwargs["headers"] = headers
    return _kwargs


def _parse_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Optional[Union[HTTPError, Schedule, ValidationError]]:
    if response.status_code == HTTPStatus.OK:
        response_200 = Schedule.from_dict(response.json())

        return response_200
    if response.status_code == HTTPStatus.UNPROCESSABLE_ENTITY:
        response_422 = ValidationError.from_dict(response.json())

        return response_422
    if response.status_code == HTTPStatus.UNAUTHORIZED:
        response_401 = HTTPError.from_dict(response.json())

        return response_401
    if response.status_code == HTTPStatus.NOT_FOUND:
        response_404 = HTTPError.from_dict(response.json())

        return response_404
    if client.raise_on_unexpected_status:
        raise errors.UnexpectedStatus(response.status_code, response.content)
    else:
        return None


def _build_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Response[Union[HTTPError, Schedule, ValidationError]]:
    return Response(
        status_code=HTTPStatus(response.status_code),
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(client=client, response=response),
    )


def sync_detailed(
    equipment_id: str,
    *,
    client: AuthenticatedClient,
    body: Schedule,
) -> Response[Union[HTTPError, Schedule, ValidationError]]:
    """Create Schedule

    Args:
        equipment_id (str):
        body (Schedule):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[Union[HTTPError, Schedule, ValidationError]]
    """

    kwargs = _get_kwargs(
        equipment_id=equipment_id,
        body=body,
    )

    response = client.get_httpx_client().request(
        **kwargs,
    )

    return _build_response(client=client, response=response)


def sync(
    equipment_id: str,
    *,
    client: AuthenticatedClient,
    body: Schedule,
) -> Optional[Union[HTTPError, Schedule, ValidationError]]:
    """Create Schedule

    Args:
        equipment_id (str):
        body (Schedule):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Union[HTTPError, Schedule, ValidationError]
    """

    return sync_detailed(
        equipment_id=equipment_id,
        client=client,
        body=body,
    ).parsed


async def asyncio_detailed(
    equipment_id: str,
    *,
    client: AuthenticatedClient,
    body: Schedule,
) -> Response[Union[HTTPError, Schedule, ValidationError]]:
    """Create Schedule

    Args:
        equipment_id (str):
        body (Schedule):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[Union[HTTPError, Schedule, ValidationError]]
    """

    kwargs = _get_kwargs(
        equipment_id=equipment_id,
        body=body,
    )

    response = await client.get_async_httpx_client().request(**kwargs)

    return _build_response(client=client, response=response)


async def asyncio(
    equipment_id: str,
    *,
    client: AuthenticatedClient,
    body: Schedule,
) -> Optional[Union[HTTPError, Schedule, ValidationError]]:
    """Create Schedule

    Args:
        equipment_id (str):
        body (Schedule):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Union[HTTPError, Schedule, ValidationError]
    """

    return (
        await asyncio_detailed(
            equipment_id=equipment_id,
            client=client,
            body=body,
        )
    ).parsed
