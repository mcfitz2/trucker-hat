from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.validation_error_detail_location import \
        ValidationErrorDetailLocation


T = TypeVar("T", bound="ValidationErrorDetail")


@_attrs_define
class ValidationErrorDetail:
    """
    Attributes:
        location (Union[Unset, ValidationErrorDetailLocation]):
    """

    location: Union[Unset, "ValidationErrorDetailLocation"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        location: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.location, Unset):
            location = self.location.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if location is not UNSET:
            field_dict["<location>"] = location

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.validation_error_detail_location import \
            ValidationErrorDetailLocation

        d = src_dict.copy()
        _location = d.pop("<location>", UNSET)
        location: Union[Unset, ValidationErrorDetailLocation]
        if isinstance(_location, Unset):
            location = UNSET
        else:
            location = ValidationErrorDetailLocation.from_dict(_location)

        validation_error_detail = cls(
            location=location,
        )

        validation_error_detail.additional_properties = d
        return validation_error_detail

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
