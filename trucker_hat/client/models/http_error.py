from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.http_error_detail import HTTPErrorDetail


T = TypeVar("T", bound="HTTPError")


@_attrs_define
class HTTPError:
    """
    Attributes:
        detail (Union[Unset, HTTPErrorDetail]):
        message (Union[Unset, str]):
    """

    detail: Union[Unset, "HTTPErrorDetail"] = UNSET
    message: Union[Unset, str] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        detail: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.detail, Unset):
            detail = self.detail.to_dict()

        message = self.message

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if detail is not UNSET:
            field_dict["detail"] = detail
        if message is not UNSET:
            field_dict["message"] = message

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.http_error_detail import HTTPErrorDetail

        d = src_dict.copy()
        _detail = d.pop("detail", UNSET)
        detail: Union[Unset, HTTPErrorDetail]
        if isinstance(_detail, Unset):
            detail = UNSET
        else:
            detail = HTTPErrorDetail.from_dict(_detail)

        message = d.pop("message", UNSET)

        http_error = cls(
            detail=detail,
            message=message,
        )

        http_error.additional_properties = d
        return http_error

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
