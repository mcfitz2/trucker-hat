from typing import Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

T = TypeVar("T", bound="ValidationErrorDetailLocation")


@_attrs_define
class ValidationErrorDetailLocation:
    """
    Attributes:
        field_name (Union[Unset, List[str]]):
    """

    field_name: Union[Unset, List[str]] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        field_name: Union[Unset, List[str]] = UNSET
        if not isinstance(self.field_name, Unset):
            field_name = self.field_name

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if field_name is not UNSET:
            field_dict["<field_name>"] = field_name

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        field_name = cast(List[str], d.pop("<field_name>", UNSET))

        validation_error_detail_location = cls(
            field_name=field_name,
        )

        validation_error_detail_location.additional_properties = d
        return validation_error_detail_location

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
