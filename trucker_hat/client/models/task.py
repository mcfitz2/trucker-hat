import datetime
from typing import Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

T = TypeVar("T", bound="Task")


@_attrs_define
class Task:
    """
    Attributes:
        equipment_id (int):
        name (str):
        user_id (int):
        user (Union[Unset, Any]):
        equipment (Union[Unset, Any]):
        schedule (Union[Unset, Any]):
        id (Union[Unset, int]):
        schedule_id (Union[None, Unset, int]):
        completed (Union[Unset, bool]):
        completed_date (Union[None, Unset, datetime.datetime]):
        completed_at_meter (Union[None, Unset, float]):
        due_at_meter (Union[None, Unset, float]):
        due_date (Union[None, Unset, datetime.datetime]):
        notes (Union[None, Unset, str]):
    """

    equipment_id: int
    name: str
    user_id: int
    user: Union[Unset, Any] = UNSET
    equipment: Union[Unset, Any] = UNSET
    schedule: Union[Unset, Any] = UNSET
    id: Union[Unset, int] = UNSET
    schedule_id: Union[None, Unset, int] = UNSET
    completed: Union[Unset, bool] = UNSET
    completed_date: Union[None, Unset, datetime.datetime] = UNSET
    completed_at_meter: Union[None, Unset, float] = UNSET
    due_at_meter: Union[None, Unset, float] = UNSET
    due_date: Union[None, Unset, datetime.datetime] = UNSET
    notes: Union[None, Unset, str] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        equipment_id = self.equipment_id

        name = self.name

        user_id = self.user_id

        user = self.user

        equipment = self.equipment

        schedule = self.schedule

        id = self.id

        schedule_id: Union[None, Unset, int]
        if isinstance(self.schedule_id, Unset):
            schedule_id = UNSET
        else:
            schedule_id = self.schedule_id

        completed = self.completed

        completed_date: Union[None, Unset, str]
        if isinstance(self.completed_date, Unset):
            completed_date = UNSET
        elif isinstance(self.completed_date, datetime.datetime):
            completed_date = self.completed_date.isoformat()
        else:
            completed_date = self.completed_date

        completed_at_meter: Union[None, Unset, float]
        if isinstance(self.completed_at_meter, Unset):
            completed_at_meter = UNSET
        else:
            completed_at_meter = self.completed_at_meter

        due_at_meter: Union[None, Unset, float]
        if isinstance(self.due_at_meter, Unset):
            due_at_meter = UNSET
        else:
            due_at_meter = self.due_at_meter

        due_date: Union[None, Unset, str]
        if isinstance(self.due_date, Unset):
            due_date = UNSET
        elif isinstance(self.due_date, datetime.datetime):
            due_date = self.due_date.isoformat()
        else:
            due_date = self.due_date

        notes: Union[None, Unset, str]
        if isinstance(self.notes, Unset):
            notes = UNSET
        else:
            notes = self.notes

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "equipment_id": equipment_id,
                "name": name,
                "user_id": user_id,
            }
        )
        if user is not UNSET:
            field_dict["user"] = user
        if equipment is not UNSET:
            field_dict["equipment"] = equipment
        if schedule is not UNSET:
            field_dict["schedule"] = schedule
        if id is not UNSET:
            field_dict["id"] = id
        if schedule_id is not UNSET:
            field_dict["schedule_id"] = schedule_id
        if completed is not UNSET:
            field_dict["completed"] = completed
        if completed_date is not UNSET:
            field_dict["completed_date"] = completed_date
        if completed_at_meter is not UNSET:
            field_dict["completed_at_meter"] = completed_at_meter
        if due_at_meter is not UNSET:
            field_dict["due_at_meter"] = due_at_meter
        if due_date is not UNSET:
            field_dict["due_date"] = due_date
        if notes is not UNSET:
            field_dict["notes"] = notes

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        equipment_id = d.pop("equipment_id")

        name = d.pop("name")

        user_id = d.pop("user_id")

        user = d.pop("user", UNSET)

        equipment = d.pop("equipment", UNSET)

        schedule = d.pop("schedule", UNSET)

        id = d.pop("id", UNSET)

        def _parse_schedule_id(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        schedule_id = _parse_schedule_id(d.pop("schedule_id", UNSET))

        completed = d.pop("completed", UNSET)

        def _parse_completed_date(
            data: object,
        ) -> Union[None, Unset, datetime.datetime]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                completed_date_type_0 = isoparse(data)

                return completed_date_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.datetime], data)

        completed_date = _parse_completed_date(d.pop("completed_date", UNSET))

        def _parse_completed_at_meter(data: object) -> Union[None, Unset, float]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, float], data)

        completed_at_meter = _parse_completed_at_meter(
            d.pop("completed_at_meter", UNSET)
        )

        def _parse_due_at_meter(data: object) -> Union[None, Unset, float]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, float], data)

        due_at_meter = _parse_due_at_meter(d.pop("due_at_meter", UNSET))

        def _parse_due_date(data: object) -> Union[None, Unset, datetime.datetime]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                due_date_type_0 = isoparse(data)

                return due_date_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.datetime], data)

        due_date = _parse_due_date(d.pop("due_date", UNSET))

        def _parse_notes(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        notes = _parse_notes(d.pop("notes", UNSET))

        task = cls(
            equipment_id=equipment_id,
            name=name,
            user_id=user_id,
            user=user,
            equipment=equipment,
            schedule=schedule,
            id=id,
            schedule_id=schedule_id,
            completed=completed,
            completed_date=completed_date,
            completed_at_meter=completed_at_meter,
            due_at_meter=due_at_meter,
            due_date=due_date,
            notes=notes,
        )

        task.additional_properties = d
        return task

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
