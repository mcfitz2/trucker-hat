from typing import Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

T = TypeVar("T", bound="User")


@_attrs_define
class User:
    """
    Attributes:
        email (str):
        token (str):
        equipment (Union[Unset, List[Any]]):
        schedules (Union[Unset, List[Any]]):
        tasks (Union[Unset, List[Any]]):
        id (Union[Unset, int]):
    """

    email: str
    token: str
    equipment: Union[Unset, List[Any]] = UNSET
    schedules: Union[Unset, List[Any]] = UNSET
    tasks: Union[Unset, List[Any]] = UNSET
    id: Union[Unset, int] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        email = self.email

        token = self.token

        equipment: Union[Unset, List[Any]] = UNSET
        if not isinstance(self.equipment, Unset):
            equipment = self.equipment

        schedules: Union[Unset, List[Any]] = UNSET
        if not isinstance(self.schedules, Unset):
            schedules = self.schedules

        tasks: Union[Unset, List[Any]] = UNSET
        if not isinstance(self.tasks, Unset):
            tasks = self.tasks

        id = self.id

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "email": email,
                "token": token,
            }
        )
        if equipment is not UNSET:
            field_dict["equipment"] = equipment
        if schedules is not UNSET:
            field_dict["schedules"] = schedules
        if tasks is not UNSET:
            field_dict["tasks"] = tasks
        if id is not UNSET:
            field_dict["id"] = id

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        email = d.pop("email")

        token = d.pop("token")

        equipment = cast(List[Any], d.pop("equipment", UNSET))

        schedules = cast(List[Any], d.pop("schedules", UNSET))

        tasks = cast(List[Any], d.pop("tasks", UNSET))

        id = d.pop("id", UNSET)

        user = cls(
            email=email,
            token=token,
            equipment=equipment,
            schedules=schedules,
            tasks=tasks,
            id=id,
        )

        user.additional_properties = d
        return user

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
