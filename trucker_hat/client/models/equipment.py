from typing import Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

T = TypeVar("T", bound="Equipment")


@_attrs_define
class Equipment:
    """
    Attributes:
        make (str):
        model (str):
        year (int):
        meter (float):
        user_id (int):
        user (Union[Unset, Any]):
        schedules (Union[Unset, List[Any]]):
        tasks (Union[Unset, List[Any]]):
        id (Union[Unset, int]):
    """

    make: str
    model: str
    year: int
    meter: float
    user_id: int
    user: Union[Unset, Any] = UNSET
    schedules: Union[Unset, List[Any]] = UNSET
    tasks: Union[Unset, List[Any]] = UNSET
    id: Union[Unset, int] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        make = self.make

        model = self.model

        year = self.year

        meter = self.meter

        user_id = self.user_id

        user = self.user

        schedules: Union[Unset, List[Any]] = UNSET
        if not isinstance(self.schedules, Unset):
            schedules = self.schedules

        tasks: Union[Unset, List[Any]] = UNSET
        if not isinstance(self.tasks, Unset):
            tasks = self.tasks

        id = self.id

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "make": make,
                "model": model,
                "year": year,
                "meter": meter,
                "user_id": user_id,
            }
        )
        if user is not UNSET:
            field_dict["user"] = user
        if schedules is not UNSET:
            field_dict["schedules"] = schedules
        if tasks is not UNSET:
            field_dict["tasks"] = tasks
        if id is not UNSET:
            field_dict["id"] = id

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        make = d.pop("make")

        model = d.pop("model")

        year = d.pop("year")

        meter = d.pop("meter")

        user_id = d.pop("user_id")

        user = d.pop("user", UNSET)

        schedules = cast(List[Any], d.pop("schedules", UNSET))

        tasks = cast(List[Any], d.pop("tasks", UNSET))

        id = d.pop("id", UNSET)

        equipment = cls(
            make=make,
            model=model,
            year=year,
            meter=meter,
            user_id=user_id,
            user=user,
            schedules=schedules,
            tasks=tasks,
            id=id,
        )

        equipment.additional_properties = d
        return equipment

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
