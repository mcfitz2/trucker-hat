from typing import Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

T = TypeVar("T", bound="Schedule")


@_attrs_define
class Schedule:
    """
    Attributes:
        meter_interval (float):
        time_interval (int):
        name (str):
        equipment_id (int):
        user_id (int):
        equipment (Union[Unset, Any]):
        user (Union[Unset, Any]):
        tasks (Union[Unset, List[Any]]):
        id (Union[Unset, int]):
        notes (Union[None, Unset, str]):
    """

    meter_interval: float
    time_interval: int
    name: str
    equipment_id: int
    user_id: int
    equipment: Union[Unset, Any] = UNSET
    user: Union[Unset, Any] = UNSET
    tasks: Union[Unset, List[Any]] = UNSET
    id: Union[Unset, int] = UNSET
    notes: Union[None, Unset, str] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        meter_interval = self.meter_interval

        time_interval = self.time_interval

        name = self.name

        equipment_id = self.equipment_id

        user_id = self.user_id

        equipment = self.equipment

        user = self.user

        tasks: Union[Unset, List[Any]] = UNSET
        if not isinstance(self.tasks, Unset):
            tasks = self.tasks

        id = self.id

        notes: Union[None, Unset, str]
        if isinstance(self.notes, Unset):
            notes = UNSET
        else:
            notes = self.notes

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "meter_interval": meter_interval,
                "time_interval": time_interval,
                "name": name,
                "equipment_id": equipment_id,
                "user_id": user_id,
            }
        )
        if equipment is not UNSET:
            field_dict["equipment"] = equipment
        if user is not UNSET:
            field_dict["user"] = user
        if tasks is not UNSET:
            field_dict["tasks"] = tasks
        if id is not UNSET:
            field_dict["id"] = id
        if notes is not UNSET:
            field_dict["notes"] = notes

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        meter_interval = d.pop("meter_interval")

        time_interval = d.pop("time_interval")

        name = d.pop("name")

        equipment_id = d.pop("equipment_id")

        user_id = d.pop("user_id")

        equipment = d.pop("equipment", UNSET)

        user = d.pop("user", UNSET)

        tasks = cast(List[Any], d.pop("tasks", UNSET))

        id = d.pop("id", UNSET)

        def _parse_notes(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        notes = _parse_notes(d.pop("notes", UNSET))

        schedule = cls(
            meter_interval=meter_interval,
            time_interval=time_interval,
            name=name,
            equipment_id=equipment_id,
            user_id=user_id,
            equipment=equipment,
            user=user,
            tasks=tasks,
            id=id,
            notes=notes,
        )

        schedule.additional_properties = d
        return schedule

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
