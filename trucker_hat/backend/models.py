import datetime
from dataclasses import dataclass
from typing import List

import sqlalchemy
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import (Boolean, Column, DateTime, Float, Integer, String,
                        UniqueConstraint, inspect, orm)
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column, relationship

from .utils import jsonify


class Base(DeclarativeBase):
    pass


db = SQLAlchemy(model_class=Base)


@dataclass
class User(db.Model):
    __tablename__ = "user"

    email: Mapped[str]
    token: Mapped[str]
    id: Mapped[int] = Column(Integer, primary_key=True, autoincrement=True)

    equipment: Mapped[List["Equipment"]] = relationship(
        back_populates="user",
        cascade="all, delete-orphan",
        primaryjoin="Equipment.user_id == User.id",
        foreign_keys="Equipment.user_id",
    )

    schedules: Mapped[List["Schedule"]] = relationship(
        back_populates="user",
        cascade="all, delete-orphan",
        primaryjoin="Schedule.user_id == User.id",
        foreign_keys="Schedule.user_id",
    )

    tasks: Mapped[List["Task"]] = relationship(
        back_populates="user",
        cascade="all, delete-orphan",
        primaryjoin="Task.user_id == User.id",
        foreign_keys="Task.user_id",
    )

    def generate_token(self):
        # self.token = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(32))
        self.token = "token"

    def __repr__(self) -> str:
        return f"User(id={self.id!r}, {self.email!r})"

    def store_and_respond(self, db):
        try:
            db.session.add(self)
            db.session.commit()
            return jsonify(self)
        except sqlalchemy.exc.IntegrityError:
            return "Integrity Error", 400


@dataclass
class Equipment(db.Model):
    __tablename__ = "equipment"

    make: Mapped[str]
    model: Mapped[str]
    year: Mapped[int]
    meter: Mapped[float]
    id: Mapped[int] = Column(Integer, primary_key=True, autoincrement=True)
    user_id: Mapped[int] = Column(Integer, nullable=False)

    user = orm.relationship(
        "User",
        primaryjoin="Equipment.user_id == User.id",
        back_populates="equipment",
        foreign_keys=user_id,
    )

    schedules: Mapped[List["Schedule"]] = relationship(
        back_populates="equipment",
        cascade="all, delete-orphan",
        primaryjoin="Schedule.equipment_id == Equipment.id",
        foreign_keys="Schedule.equipment_id",
    )

    tasks: Mapped[List["Task"]] = relationship(
        back_populates="equipment",
        cascade="all, delete-orphan",
        primaryjoin="Task.equipment_id == Equipment.id",
        foreign_keys="Task.equipment_id",
    )
    __table_args__ = (
        UniqueConstraint("make", "model", "year", name="_customer_location_uc"),
    )

    def __repr__(self) -> str:
        return f"Equipment(id={self.id!r}, {self.year!r} {self.make!r}{self.model!r})"

    def create_task(self, **kwargs):
        session = inspect(self).session
        if (
            session.query(Task)
            .filter(
                Task.equipment_id == self.id,
                Task.name == kwargs.get("name"),
                Task.completed == False,
            )
            .first()
        ):
            # there is already an uncompleted task for this schedule and equipment
            return None
        kwargs["equipment_id"] = self.id
        task = Task(**kwargs)
        session.add(task)
        session.commit()
        return task

    def store_and_respond(self, db):
        try:
            db.session.add(self)
            db.session.commit()
            return jsonify(self)
        except sqlalchemy.exc.IntegrityError:
            return "Integrity Error", 400


@dataclass
class Schedule(db.Model):
    __tablename__ = "schedule"

    meter_interval: Mapped[float]
    time_interval: Mapped[int]  # days
    id: Mapped[int] = Column(Integer, primary_key=True, autoincrement=True)
    name: Mapped[str] = mapped_column(unique=True)
    equipment_id: Mapped[int] = Column(Integer, nullable=False)
    user_id: Mapped[int] = Column(Integer, nullable=False)

    notes: Mapped[str] = Column(String, nullable=True)

    equipment = orm.relationship(
        "Equipment",
        primaryjoin="Schedule.equipment_id == Equipment.id",
        back_populates="schedules",
        foreign_keys=equipment_id,
    )

    user = orm.relationship(
        "User",
        primaryjoin="Schedule.user_id == User.id",
        back_populates="schedules",
        foreign_keys=user_id,
    )

    tasks: Mapped[List["Task"]] = relationship(
        back_populates="schedule",
        cascade="all, delete-orphan",
        primaryjoin="Task.schedule_id == Schedule.id",
        foreign_keys="Task.schedule_id",
    )

    def most_recent_task(self):
        session = inspect(self).session
        return (
            session.query(Task)
            .filter(
                Task.schedule_id == self.id,
                Task.equipment_id == self.equipment_id,
                Task.completed == True,
            )
            .order_by(Task.completed_date.desc())
            .first()
        )

    def process(self):
        most_recent = self._most_recent_task()
        if not most_recent:
            self.create_task(due_date=datetime.datetime.now())
        else:
            if self.equipment.meter:
                if self.equipment.meter > (
                    most_recent.completed_at_meter + self.meter_interval
                ):
                    self.create_task(
                        due_at_meter=most_recent.completed_at_meter
                        + self.meter_interval
                    )
                elif datetime.datetime.now() > (
                    most_recent.completed_date
                    + datetime.timedelta(days=self.time_interval)
                ):
                    self.create_task(
                        due_date=most_recent.completed_date
                        + datetime.timedelta(days=self.time_interval)
                    )

    def create_task(self, due_at_meter=None, due_date=None):
        session = inspect(self).session
        if (
            session.query(Task)
            .filter(
                Task.schedule_id == self.id,
                Task.equipment_id == self.equipment_id,
                Task.completed == False,
            )
            .first()
        ):
            # there is already an uncompleted task for this schedule and equipment
            return
        session.add(
            Task(
                equipment_id=self.equipment_id,
                schedule_id=self.id,
                due_at_meter=due_at_meter,
                due_date=due_date,
                name=self.name,
                notes=self.notes,
            )
        )
        session.commit()

    def store_and_respond(self, db):
        if (
            db.session.query(Equipment.id)
            .filter(Equipment.id == self.equipment_id)
            .first()
        ):
            try:
                db.session.add(self)
                db.session.commit()
                self.process()
                return jsonify(self)
            except sqlalchemy.exc.IntegrityError:
                return "Integrity Error", 400
        return "Equipment does not exist", 400

    def __repr__(self) -> str:
        return f"Schedule(id={self.id!r}, name={self.name!r})"


@dataclass
class Task(db.Model):
    __tablename__ = "task"

    id: Mapped[int] = Column(Integer, primary_key=True, autoincrement=True)
    equipment_id: Mapped[int] = Column(Integer, nullable=False)
    schedule_id: Mapped[int] = Column(Integer, nullable=True)
    completed: Mapped[bool] = Column(Boolean, nullable=False, default=False)
    completed_date: Mapped[datetime.datetime] = Column(
        DateTime, nullable=True, default=None
    )
    completed_at_meter: Mapped[float] = Column(Float, nullable=True)
    due_at_meter: Mapped[float] = Column(Float, nullable=True)
    due_date: Mapped[datetime.datetime] = Column(DateTime, nullable=True, default=None)
    name: Mapped[str] = Column(String, nullable=False)
    notes: Mapped[str] = Column(String, nullable=True)
    user_id: Mapped[int] = Column(Integer, nullable=False)
    user = orm.relationship(
        "User",
        primaryjoin="Task.user_id == User.id",
        back_populates="tasks",
        foreign_keys=user_id,
    )
    equipment = orm.relationship(
        "Equipment",
        primaryjoin="Task.equipment_id == Equipment.id",
        back_populates="tasks",
        foreign_keys=equipment_id,
    )

    schedule = orm.relationship(
        "Schedule",
        primaryjoin="Task.schedule_id == Schedule.id",
        back_populates="tasks",
        foreign_keys=schedule_id,
    )

    def store_and_respond(self, db):
        if (
            db.session.query(Equipment.id)
            .filter(Equipment.id == self.equipment_id)
            .first()
        ):
            if (
                self.schedule_id
                and db.session.query(Equipment.id)
                .filter(Equipment.id == self.equipment_id)
                .first()
                or self.schedule_id is None
            ):
                try:
                    db.session.add(self)
                    db.session.commit()
                    return jsonify(self)
                except sqlalchemy.exc.IntegrityError:
                    return "Integrity Error", 400
            return "Schedule does not exist", 400
        return "Equipment does not exist", 400

    def __repr__(self) -> str:
        return f"Task(id={self.id!r})"
