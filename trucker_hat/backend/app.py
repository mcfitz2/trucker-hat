import datetime

from apiflask import APIFlask, HTTPTokenAuth
from flask import request

from .models import Equipment, Schedule, Task, User, db
from .utils import get_json, jsonify
from .views import EquipmentSchema, ScheduleSchema, TaskSchema, UserSchema

app = APIFlask(__name__, spec_path="/openapi.yaml")
app.config["SCHEDULER_API_ENABLED"] = True
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite://"
app.config["SPEC_FORMAT"] = "yaml"
app.security_schemes = {  # equals to use config SECURITY_SCHEMES
    "ApiKeyAuth": {
        "type": "apiKey",
        "in": "header",
        "name": "X-API-Token",
    }
}
auth = HTTPTokenAuth(scheme="ApiKey", header="X-API-Token")


@auth.verify_token
def verify_token(token):
    u = None
    if token:
        u = User.query.filter(User.token == token).first()
    return u


@app.route("/user", methods=["POST"])
@app.input(UserSchema)
@app.output(UserSchema)
def create_user():
    body = get_json(request)
    user = User(**body)
    return user.store_and_respond(db)


@app.route("/user", methods=["GET"])
@app.output(UserSchema(many=True))
@app.doc(security="ApiKeyAuth")
def list_users():
    users = User.query.all()
    return jsonify(users)


@app.route("/user/<user_id>", methods=["PATCH"])
@app.input(UserSchema)
@app.output(UserSchema)
@app.doc(security="ApiKeyAuth")
def patch_user(user_id):
    body = get_json(request)

    user = User.query.get(user_id)
    for key, value in body.items():
        if hasattr(user, key):
            setattr(user, key, value)
    db.session.commit()
    return jsonify(user)


@app.route("/user/<user_id>", methods=["DELETE"])
@app.output(UserSchema)
@app.doc(security="ApiKeyAuth")
def delete_user(user_id):
    User.query.delete(user_id)
    db.session.commit()
    return


@app.route("/user/<user_id>", methods=["GET"])
@app.output(UserSchema)
@app.doc(security="ApiKeyAuth")
def get_user(user_id):
    user = User.query.get(user_id)
    return jsonify(user)


@app.route("/equipment", methods=["POST"])
@app.input(EquipmentSchema)
@app.output(EquipmentSchema)
@app.doc(security="ApiKeyAuth")
def create_equipment():
    body = get_json(request)
    body["user_id"] = 1
    equipment = Equipment(**body)
    db.session.add(equipment)
    db.session.commit()
    return jsonify(equipment)


@app.route("/equipment", methods=["GET"])
@app.output(EquipmentSchema(many=True))
@app.doc(security="ApiKeyAuth")
def list_equipment():
    equipment = Equipment.query.all()
    return jsonify(equipment)


@app.route("/equipment/<equipment_id>", methods=["PATCH"])
@app.input(EquipmentSchema)
@app.output(EquipmentSchema)
@app.doc(security="ApiKeyAuth")
def patch_equipment(equipment_id):
    body = get_json(request)

    equipment = Equipment.query.get(equipment_id)
    for key, value in body.items():
        if hasattr(equipment, key):
            setattr(equipment, key, value)
    db.session.commit()
    return jsonify(equipment)


@app.route("/equipment/<equipment_id>", methods=["DELETE"])
@app.output(EquipmentSchema)
@app.doc(security="ApiKeyAuth")
def delete_equipment(equipment_id):
    Equipment.query.delete(equipment_id)
    db.session.commit()
    return


@app.route("/equipment/<equipment_id>", methods=["GET"])
@app.output(EquipmentSchema)
@app.doc(security="ApiKeyAuth")
def get_equipment(equipment_id):
    equipment = Equipment.query.get(equipment_id)
    return jsonify(equipment)


@app.route("/equipment/<equipment_id>/schedule", methods=["POST"])
@app.input(ScheduleSchema)
@app.output(ScheduleSchema)
@app.doc(security="ApiKeyAuth")
def create_schedule(equipment_id):
    body = get_json(request)
    if not body.get("equipment_id"):
        body["equipment_id"] = equipment_id
    schedule = Schedule(**body)
    return schedule.store_and_respond(db)


@app.route("/equipment/<equipment_id>/schedule/<schedule_id>/task", methods=["POST"])
@app.input(TaskSchema)
@app.output(TaskSchema)
@app.doc(security="ApiKeyAuth")
def create_schedule_task(equipment_id, schedule_id):
    body = get_json(request)

    if not body.get("equipment_id"):
        body["equipment_id"] = equipment_id
    if not body.get("schedule_id"):
        body["schedule_id"] = schedule_id
    if body.get("completed") and body["completed"]:
        body["completed_date"] = datetime.datetime.now()
    task = Task(**body)
    return task.store_and_respond(db)


@app.route("/equipment/<equipment_id>/task", methods=["POST"])
@app.input(TaskSchema)
@app.output(TaskSchema)
@app.doc(security="ApiKeyAuth")
def create_task(equipment_id):
    body = get_json(request)

    if not body.get("equipment_id"):
        body["equipment_id"] = equipment_id
    if body.get("completed") and body["completed"]:
        body["completed_date"] = datetime.datetime.now()
    if not body.get("due_date"):
        body["due_date"] = datetime.datetime.now()
    equipment = Equipment.query.get(equipment_id)
    if equipment:
        task = equipment.create_task(**body)
        if task:
            return jsonify(task)
        else:
            return "failed to create task, does it already exist with this name?", 500
    else:
        return "Equipment does not exist", 400


@app.route("/task/<task_id>/complete", methods=["POST"])
@app.input(TaskSchema)
@app.output(TaskSchema)
@app.doc(security="ApiKeyAuth")
def complete_task(task_id):
    body = get_json(request)
    task = Task.query.get(task_id)
    task.completed = True
    if body.get("completed_date"):
        task.completed_date = body["completed_date"]
    else:
        task.completed_date = datetime.datetime.now()
    if body.get("completed_at_meter"):
        task.completed_at_meter = body["completed_at_meter"]
    else:
        task.completed_at_meter = task.equipment.meter
    db.session.commit()
    return jsonify(task)


@app.route(
    "/equipment/<equipment_id>/schedule/<schedule_id>/last_task", methods=["GET"]
)
@app.output(TaskSchema)
@app.doc(security="ApiKeyAuth")
def get_last_task(equipment_id, schedule_id):
    schedule = (
        db.session.query(Schedule)
        .filter(Schedule.equipment_id == equipment_id, Schedule.id == schedule_id)
        .one()
    )
    task = schedule.most_recent_task()
    if not task:
        return "No completed tasks", 404
    else:
        return jsonify(schedule.most_recent_task())


db.init_app(app)
app.db = db
with app.app_context():
    db.create_all()
    user = User(email="mcfitz@gmail.com")
    user.generate_token()
    db.session.add(user)
    db.session.commit()
