import datetime
import json
from dataclasses import asdict, is_dataclass

from flask import make_response


class JSONExtendDecoder(json.JSONDecoder):
    def __init__(self, *args, **kwargs):
        super().__init__(object_hook=self.try_datetime, *args, **kwargs)

    @staticmethod
    def try_datetime(d):
        ret = {}
        for key, value in d.items():
            try:
                ret[key] = datetime.datetime.fromisoformat(value)
            except (ValueError, TypeError):
                ret[key] = value
        return ret


class JsonExtendEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime.datetime):
            return o.isoformat()
        elif isinstance(o, datetime.date):
            return o.isoformat()
        elif is_dataclass(o):
            return asdict(o)
        else:
            return json.JSONEncoder.default(self, o)


def jsonify(obj, error_code=200):
    response = make_response(
        json.dumps(obj, cls=JsonExtendEncoder),
        error_code,
    )
    response.headers["Content-Type"] = "application/json"
    return response


def get_json(request):
    return json.loads(request.data, cls=JSONExtendDecoder)
