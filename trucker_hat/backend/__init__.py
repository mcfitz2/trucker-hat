from trucker_hat.backend.app import app


def run():
    app.run(host="0.0.0.0", debug=True)
