from marshmallow_sqlalchemy import SQLAlchemyAutoSchema

from .models import Equipment, Schedule, Task, User


class UserSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = User
        include_relationships = True
        load_instance = True


class EquipmentSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Equipment
        include_relationships = True
        load_instance = True


class ScheduleSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Schedule
        include_relationships = True
        load_instance = True


class TaskSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Task
        include_relationships = True
        load_instance = True
