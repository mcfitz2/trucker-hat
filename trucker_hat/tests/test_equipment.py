import pytest

from trucker_hat.backend.app import app as real_app
from trucker_hat.backend.models import Equipment


@pytest.fixture()
def app():
    yield real_app


@pytest.fixture
def client(app):
    client = app.test_client()
    yield client


@pytest.fixture
def context(app):
    context = app.app_context()
    yield context


@pytest.fixture
def truck():
    yield Equipment(
        **{"year": 2012, "make": "Chevrolet", "model": "Silverado 1500", "meter": 0}
    )


def test_create_equipment(app, client, context):
    response = client.post(
        "/equipment",
        json={"year": 2012, "make": "Chevrolet", "model": "Silverado 1500", "meter": 0},
    )
    print(response.json)
    assert response.json["year"] == 2012
    assert response.json["make"] == "Chevrolet"
    assert response.json["model"] == "Silverado 1500"
    assert response.json["meter"] == 0.0
    assert response.json["schedules"] == []
    assert response.json["tasks"] == []
    assert response.json["id"] == 1


def test_get_equipment(app, client, context, truck):
    with context:
        app.db.session.add(truck)
        app.db.session.commit()
    response = client.get("/equipment/1")

    assert response.json["year"] == 2012
    assert response.json["make"] == "Chevrolet"
    assert response.json["model"] == "Silverado 1500"
    assert response.json["meter"] == 0.0


def test_delete_equipment(app, client, context, truck):
    with context:
        app.db.session.add(truck)
        app.db.session.commit()
    response = client.delete("/equipment/1")

    assert response.status_code == 200
    response = client.get("/equipment/1")

    assert response.status_code == 404


def test_update_equipment(app, client, context, truck):
    with context:
        app.db.session.add(truck)
        app.db.session.commit()

    response = client.patch(
        "/equipment/1",
        json={
            "year": 2013,
            "make": "Chevrolet",
            "model": "Silverado 1500",
            "meter": 1000,
        },
    )
    assert response.json["year"] == 2013
    assert response.json["make"] == "Chevrolet"
    assert response.json["model"] == "Silverado 1500"
    assert response.json["meter"] == 1000
    assert response.json["schedules"] == []
    assert response.json["tasks"] == []
    assert response.json["id"] == 1
